import json

from flask import Flask, jsonify
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


class HelloWorld(Resource):
    def get (self):
        return {"Hello":"World"}

class UserData(Resource): 
    def get (self):
        return  UserData.read_json_users()
    
    def read_json_users():
        with open("user_data.json") as f:
            users = json.load(f)
        f.close()
        return jsonify(users)
   
class UsersPosts(Resource):
    """
    : returns json data
    """
    def get (self):
        return UsersPosts.read_json_posts()
    
    def read_json_posts():
        with open("user_posts.json") as f:
            posts = json.load(f)
        f.close()
        return jsonify(posts)

api.add_resource(HelloWorld, '/')
api.add_resource(UserData, '/users')
api.add_resource(UsersPosts, '/posts')

if __name__ == '__main__':
    app.run (debug=True, host='0.0.0.0')

# docker build -t users_posts_image:latest .
# docker run -d -p 5000:5000 users_posts_image