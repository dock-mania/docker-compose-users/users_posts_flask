FROM python:3-alpine3.16

COPY . /app

WORKDIR /app

RUN pip install --verbose -r requirements.txt

ENTRYPOINT ["python"]

CMD ["app.py"]